<?php
ini_set('display_errors', 1);
require_once 'Dao/ProdutoDao.php';


$produtoDao = new ProdutoDao();

$produtos = $produtoDao->listarProdutos();
echo '<pre>';
print_r($produtos);
echo '</pre>';

?>
<!DOCTYPE html>
<html>
<?php
require_once 'head.php';
?>

<body>
    <?php
    require_once 'menu_superior.php';
    ?>

    <hr>
    <h1>Listagem dos produtos</h1>
    <div class="container">
        <div class="row">
            <?php foreach ($produtos as $produto) { ?>
                <div class="col-md-4">
                    ID: <?= $produto->id ?><br>
                    Nome: <?= $produto->nome ?><br>
                    Preço: <?= $produto->preco ?><br>
                    Quantidade: <?= $produto->quantidade ?>
                </div>
            <?php } ?>
        </div>
    </div>
    <?php
    require_once 'footer.php';
    ?>
</body>

</html>