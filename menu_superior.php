

<ul class="nav nav-pills">
  <li class="nav-item">
    <a class="nav-link active" href="#">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Produtos</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="#">Compras</a>
  </li>
  <li class="nav-item dropdown">
    <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" 
    aria-haspopup="true" aria-expanded="false">Administrador</a>
    <div class="dropdown-menu">
      <a class="dropdown-item" href="#">Cadastrar</a>
      <a class="dropdown-item" href="#">Alterar</a>
      <a class="dropdown-item" href="#">Excluir</a>
      <div class="dropdown-divider"></div>
      <a class="dropdown-item" href="#">Link isolado</a>
    </div>
  </li>

  <li class="nav-item">
    <a class="nav-link disabled" href="#">Sair</a>
  </li>
</ul>