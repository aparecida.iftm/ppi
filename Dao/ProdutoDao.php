<?php
require_once $_SERVER['DOCUMENT_ROOT'] . "/Util/Conexao.php"; // statico
// require_once '../Util/Conexao.php';

class ProdutoDao
{

    public function __construct()
    {
        
    }

    public function salvar($produto)
    {
        try {
            $sql = 'insert into produtos (nome, preco, quantidade)
         values (:nome, :preco, :quantidade)';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->bindValue(':nome', $produto['nome']);
            $p_sql->bindValue(':preco', $produto['preco']);
            $p_sql->bindValue(':quantidade', $produto['quantidade']);
            $p_sql->execute();
            echo 'tudo certo';
            return;
        } catch (Exception $e) {
            print_r($e);
        }
    }

    public function listarProdutos(){
        try {
            $sql = 'select * from produtos';
            $p_sql = Conexao::getInstancia()->prepare($sql);
            $p_sql->execute();
            return $p_sql->fetchAll(PDO::FETCH_OBJ);
        } catch (Exception $e) {
            print_r($e);
        }
    }
}
